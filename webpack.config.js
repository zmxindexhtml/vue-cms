const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin');
module.exports = {
     entry: path.join(__dirname,"./src/main.js"),//入口文件地址
     output: {
         path: path.join(__dirname,"./dist"),//编译出口文件地址
         filename: "bundle.js"
     },
     mode: "development",
     plugins: [
            new htmlWebpackPlugin({
                template:path.join(__dirname,'./src/index.html'),
                filename:"index.html"
            }),
            new VueLoaderPlugin()
     ],
     module: {//配置loader规则
         rules: [
              //配置css-loader,style-loader样式匹配规则
             {test:/\.css$/,use:['style-loader','css-loader']},
             {test:/\.less$/,use:['style-loader','css-loader','less-loader']},
             {test:/\.scss$/,use:['style-loader','css-loader','sass-loader']},
             {test:/\.(jpg|jpeg|gif|png)/,use: 'url-loader'},
             {test:/\.(svg|woff|ttf|eot|woff2)/,use: 'url-loader'},
             {test:/\.js$/,use:'babel-loader',exclude:/node_modules/},//配置ES6转换ES5 Babel 
             {test:/\.vue$/,use:'vue-loader'}//处理vue文件的loader
             
         ]
     }
   
}