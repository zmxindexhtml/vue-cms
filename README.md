# 这是一个Vue框架做的移动端项目

## 用了mui，mint-ui组件

### 项目不是很完善，多多包涵

## [开原协议之间的区别](https://www.oschina.net/question/12_2664)

## 用命令行把修改后的代码上传到码云？

1. git add .
2. git commit -m "提交信息"
3. git push

## 制作首页App组件

1.完成Header区域，使用的是Mint-UIz中的Header组件

2.制作底部的Tabbar区域，使用的是MUi的Tabbar.htm

     +在制作 购物车 小图标的时候，操作会繁杂一些

     +先把 扩展图标的 css样式，拷贝到项目中

     +拷贝 扩展字体库 ttf文件项目中

3.要在 中间区域放置一个 router-view 来展示路由匹配到的组件

## 改造tabbar 为 router-link

## 设置路由高亮

## 点击tabbar的路由链接，展示对应的路由组件

## 制作首页轮播图布局

## 加载轮播图数据

1.获取数据，使用vue-resource发送ajax

2.使用vue-resource的this.$.http.get获取数据

3.获取到的数据，要保存在data身上

4.使用v-for循环渲染每个item项

## 改造六宫格区域样式

## 改造新闻资讯链接

## 新闻资讯页面制作

1.绘制界面 使用MUI中的Midea.html绘制页面

2.使用vue-resource获取数据

3.渲染真实数据

## 实现新闻资讯列表 点击跳转答新群详情

1.把列表中的每一项改造为 router-link 同时在跳转的时候应该提供唯一的ID标识符

2.创建新闻详情的组件页面 NewsInfo.vue

3.在路由模块中，将新闻详情的路由地址和组件页面对应起来

## 实现 新闻详情 的 页面布局 和数据渲染 

## 单独封装一个 comment.vue 评论子组件

1. 先创建一个单独的 comment.vue 组件模板
   
2.在需要使用 comment 组件的页面中，先手动 导入 comment组件
     + 'import comment from './comment.vue'
  
3.在父组件中，使用'components'属性，将刚才导入的comment组件，注册为自己的子组件

4.将注册自组件时候的 注册名字 以标签行驶 在页面中引用即可

## 获取所有的评论数据显示到页面中



## 实现点击加载更多评论的功能

1.为加载更多按钮绑定点击事件，在事件中，请求下一页数据

2.点击加载更多，让 pageIndex++，然后重新调用  this.getcomments()即可

3.为了防止新数据覆盖老数据的情况，我们在点击的时候，每当获取到新数据，应该让老数据调用数组的concat方法，拼接上新数组


## 发表评论

1.把文本框作双向数据绑定

2.为发表按钮绑定一个事件

3.校验评论内容是否为空，如果为空提示用户重新输入

## 绘制图片列表 组件页面结构并美化样式

1. 制作 顶部的滑动条
2. 制作 底部的图片列表

## 制作顶部滑动条的坑：

1. 需要借助 mui 中的 tab-top-webview-main.html
2. 需要把 slider 区域的 mui-fullscreen 类去掉
3. 滑动条无法正常触发滑动，通过检查官方文档，发现都是JS组件，需要被初始化一下：
   + 导入 mui.js
   + 调用官方提供的 方式 去初始化：
  ```
     mui('.mui-scroll-wrapper').scroll({
          deceleration: 0.0005 //flickk 减速系数，系数越大，滚动速度越慢，滚动距离越小，默认值0.0006
     })

  ```
4. 我们在初始化 滚动条 的时候，导入的mui.js 但是 控制台报错： ‘UNcaught TypeError: 'caller', 'callee', and 
   'arguments' properties my not be accessed on strict mode'
   + 经过我们合理的推测， 觉得 可能是 mui.js 中用到了 'caller'，'callee', and 'arguments' 东西，但是 webpack 打包好的 bundle.js中，默认是启用严格模式的，所以，这两者冲突了
   + 解决方案： 1. 把 mui.js 中的 非严格 模式的代码改掉，但是不现实； 2.把webpack 打包时候的严格模式禁用掉；
   + 最终 我们选择了 plan B 移除严格模式：使用这个插件 babel-plugin-transform-remove-strict-mode

5. 刚进入 图片分享页面的时候， 滑动条无法正常工作，经过我们认真的分析，发现，如果要初始化， 滑动条，必须要等 DOM
   元素加载完毕，所以我们把 初始化 滑动条的代码，搬到了 mounted 生命周期函数中

6.当 滑动条 调试OK后，发现，tabbar 无法正常工作了，这时候，我们需要把每个 tabbar 按钮的样式中 .'mui-tab-item' 重新改一下名字

7. 获取所有分类，并渲染 分类列表
   

## 制作图片列表区域
1. 图片列表需要使用懒加载技术，我们可以使用 Mint-ui 提供的组件 ‘lazy-load’
2. 根据 'lazy-load'的使用文档

