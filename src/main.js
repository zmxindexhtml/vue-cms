//入口文件

import Vue from "vue";

//导入路由模块
import VueRouter from "vue-router";

import "mint-ui/lib/style.css" //导入mint-ui组件样式文

import app from "./App.vue" //导入APP根组件

//导入自己的router.js路由模块
import router from "./router.js"

import "./lib/mui/css/mui.min.css"

//导入mui的扩展图标样式文件
import "./lib/mui/css/icons-extra.css"

//导入vue-resource包并注册
import VueResource from "vue-resource"
Vue.use(VueResource)

//设置请求的根路径
Vue.http.options.root = 'http://www.lovegf.cn:8899';

//全局设置post表单提交的数据格式
Vue.http.options.emulateJSON = true;


//按需导入mint-ui组件
import {
  Header,
  Swipe,
  SwipeItem,
  Button,
  Lazyload
} from 'mint-ui';
Vue.component(Header.name, Header) //注册Header组件
Vue.component(Swipe.name, Swipe)
Vue.component(SwipeItem.name, SwipeItem)
Vue.component(Button.name, Button)
Vue.component(Lazyload)

Vue.use(VueRouter) //注册路由模块

//导入moment.js包
import moment from "moment"

// 定义一个全局过滤器
Vue.filter('dateFormat', (data, arg) => {
  return moment(data).format(arg)
})




var vm = new Vue({
  el: '#app',
  router, //挂在路由对象
  render: c => c(app)
})