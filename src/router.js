import Vuerouter from "vue-router"

//导入对应的路由组件
import HomeContainer from "./components/tabbar/HomeContainer.vue"
import MemberContainer from "./components/tabbar/MemberContainer.vue"
import ShopcarContainer from "./components/tabbar/ShopcarContainer.vue"
import SearchContainer from "./components/tabbar/SearchContainer.vue"
import NewsList from "./components/news/NewsList.vue"
import NewsInfo from "./components/news/NewsInfo.vue"
import Photoshare from "./components/photos/Photoshare.vue"

var router = new Vuerouter({
    routes: [//配置路由规则
        {path: '/', redirect: '/home'},
        {path: '/home',component: HomeContainer},
        {path: '/member',component: MemberContainer},
        {path: '/shopcar',component: ShopcarContainer},
        {path: '/search',component: SearchContainer},
        {path: '/home/newsList',component: NewsList},
        {path: '/home/newsinfo/:id',component: NewsInfo},
        {path: '/home/photoShare',component: Photoshare}
        
    ],
    linkActiveClass: 'mui-active'//覆盖默认的路由高亮的类，默认的类是router-link-acitive
})
export default router